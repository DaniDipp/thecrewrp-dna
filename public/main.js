var activeTimeout = 0;
var progress_id = 0;

function keyDown(event){
    if(progress_id !== 0){
        clearInterval(progress_id);
        progress_id = 0;
        
        var bar = document.getElementById("bar");
        bar.style.width = "0";
        bar.innerHTML = "";
    }
    
    if(event.keyCode === 13){
        event.preventDefault();
        convert(!(/^[AaCcGgTt]+$/).test(event.target.value));
    }
}

function convert(mode){
    var input = document.getElementById("input");
    document.getElementById("output").value = "";
    result = (mode) ? textToDna(input.value.trim()) : dnaToText(input.value.trim());
    
    if(result) {
        progress(function(){ document.getElementById("output").value = result; });
    }
    else {
        var answer = document.getElementById("answer");
        answer.innerHTML = "Invalid characters in input field. Please use "+(mode ? 'letters, numbers, space and "-"' : "A, C, G, T")+".";
        answer.style.backgroundColor = "red";
        clearTimeout(activeTimeout);
        activeTimeout = setTimeout(function() {
            var answer = document.getElementById("answer");
            answer.innerHTML = "";
            answer.style.backgroundColor = "";
        }, 3000);
    }
}

function swap(){
    var input = document.getElementById("input");
    var output = document.getElementById("output");
    
    var temp = input.value;
    input.value = output.value;
    output.value = temp;
}

function copyOutput(){
    var output = document.getElementById("output");
    var answer = document.getElementById("answer");
    
    if (output.value == "") return;
    output.select();
    answer.style.backgroundColor = "";
    try {
        // The important part (copy selected text)
        var ok = document.execCommand('copy');

        if (ok) answer.innerHTML = 'Copied!';
        else    answer.innerHTML = 'Unable to copy!';
    } catch (err) {
        answer.innerHTML = 'Browser does not support auto copy.';
    }
    clearTimeout(activeTimeout);
    activeTimeout = setTimeout(function() {document.getElementById("answer").innerHTML = ""}, 1000);
}

function progress(callbackFunction) {
    if (progress_id == 0) {
        var bar = document.getElementById("bar");
        var width = 1;
        progress_id = setInterval(frame, 5);
        function frame() {
            if (width >= 100) {
                clearInterval(progress_id);
                progress_id = 0;
                callbackFunction();
            } else {
                width += (1/(0.01*width + 0.04))*0.04;
                bar.style.width = width + "%";
                bar.innerHTML = width.toFixed(1) + "%";
            }
            
        }
    }
}
const ALPHABET = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","0","1","2","3","4","5","6","7","8","9"," ","-",];

function textToDna(fromText){
    if(!fromText) return false;                                                                     //abort empty string
    
    var toDna = "";
    for(const character of fromText){                                                               //iterate over each character
        const index = ALPHABET.indexOf(character);                                                  //encode character
        if(index === -1) return false;                                                              //abort if character is not encodable
        var base4 = index.toString(4).padStart(3, "0");                                             //convert code to base 4
        toDna += base4.replace(/0/g,"A").replace(/1/g,"C").replace(/2/g,"G").replace(/3/g,"T");     //represent as DNA (ACGT characters) and add it to the return string
    }
    return toDna;
}

function dnaToText(fromDna){
    if(!fromDna) return false;                                                                      //abort empty string
    
    var toText = "";
    fromDna = fromDna.match(/.{1,3}/g);                                                             //split input string into chunks of 3 characters
    for(var chunk of fromDna){                                                                      //iterate over chunks
        chunk = chunk.replace(/[Aa]/g,"0").replace(/[Cc]/g,"1").replace(/[Gg]/g,"2").replace(/[Tt]/g,"3");      //convert to number representation
        if(! (/^[0123]+$/).test(chunk) ) return false;                                              //abort if some digits can't be converted
        const index = parseInt(chunk, 4);                                                           //parse base 4 to decimal (base 10)
        toText += ALPHABET[index];                                                                  //get character from ALPHABET and add it to the return string
    }
    return toText;
}